import {
  Component,
  OnInit,
  HostListener,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { Select, Store } from '@ngxs/store';
import {
  EDIT_DATA,
  POPULATE_DATA,
  SEARCH_JOB,
  SEARCH_JOB_DEPT,
} from 'src/app/ngxs/action';
import { JobState } from 'src/app/ngxs/state';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  @ViewChild('mymodal') mymodal: ElementRef;

  searchForm: FormGroup;

  editForm: FormGroup;

  @Select(JobState.allDataCount) allDataCount$;

  @Select(JobState.marketingData) marketingData$;
  @Select(JobState.marketingDataCount) marketingCount$;

  @Select(JobState.othersData) othersData$;
  @Select(JobState.othersDataCount) otherCount$;

  selectDepartment: any;
  selectCountry: any;
  jobType: any;

  mode = 'desktop';

  screenWidth: any;
  screenHeight: any;
  bannerImage: any;

  private unsubscribe$ = new Subject();

  constructor(
    private store: Store,
    private loading: LoadingController,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private common: CommonService
  ) {}

  async ngOnInit() {
    this.searchForm = this.fb.group({
      searchValue: ['', Validators.required],
    });

    this.editForm = this.fb.group({
      title: ['', Validators.required],
      location: ['', Validators.required],
      level: ['', Validators.required],
    });
    const loader = await this.loading.create({
      message: 'Getting everything ready...',
      spinner: 'bubbles',
    });
    loader.present();
    this.store
      .dispatch(new POPULATE_DATA())
      .toPromise()
      .then(() => {
        loader.dismiss();
      });
    this.valueChange();
  }

  valueChange() {
    this.searchForm.controls.searchValue.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((valueChanges) => {
        console.log(valueChanges);
        if (valueChanges == '' || valueChanges == null) {
          this.store.dispatch(new POPULATE_DATA());
        } else {
          this.store
            .dispatch(new POPULATE_DATA())
            .toPromise()
            .then(() => {
              this.store.dispatch(new SEARCH_JOB(valueChanges));
            });
        }
      });
  }

  departmentChange(event) {
    // console.log(event.detail.value);
    const dept = event.detail.value;
    this.store.dispatch(new SEARCH_JOB_DEPT(dept));
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  editData() {
    const dataToEdit = this.editForm.getRawValue();
    const allData = this.store.selectSnapshot((state) => state.JobState.data);
    let findData = allData.filter((res) => {
      return res.index == this.common.indexInd;
    });
    let currentData = {
      ...findData[0],
      ...dataToEdit,
    };
    console.log('zzzzzzz', currentData);
    this.store.dispatch(new EDIT_DATA(currentData));
    this.modalService.dismissAll();
  }

  openTheModal() {
    const currentData = this.common.dataBuff;
    console.log('current data', currentData);
    console.log('INDEX', this.common.indexInd);

    this.editForm.controls.title.setValue(currentData?.title);
    this.editForm.controls.location.setValue(currentData?.location);
    this.editForm.controls.level.setValue(currentData?.level);

    this.modalService
      .open(this.mymodal, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          // this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
}
