import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiddlebgComponent } from './middlebg.component';

describe('MiddlebgComponent', () => {
  let component: MiddlebgComponent;
  let fixture: ComponentFixture<MiddlebgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiddlebgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiddlebgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
