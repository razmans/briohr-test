import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { ListItemsComponent } from './list-items/list-items.component';
import { MiddlebgComponent } from './middlebg/middlebg.component';
import { FooterComponent } from './footer/footer.component';
import { MatIconModule } from '@angular/material/icon';
@NgModule({
  declarations: [
    HeaderComponent,
    ListItemsComponent,
    MiddlebgComponent,
    FooterComponent,
  ],
  imports: [CommonModule, IonicModule, MatIconModule],
  exports: [
    HeaderComponent,
    ListItemsComponent,
    MiddlebgComponent,
    FooterComponent,
  ],
  entryComponents: [
    HeaderComponent,
    ListItemsComponent,
    MiddlebgComponent,
    FooterComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ComponentModule {}
