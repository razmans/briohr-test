import { EventEmitter, Output } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngxs/store';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss'],
})
export class ListItemsComponent implements OnInit {
  @Input() item;
  @Output() openModal = new EventEmitter();

  constructor(
    private modal: ModalController,
    private common: CommonService,
    private store: Store
  ) {}

  ngOnInit(): void {
    // console.log('item', this.item);
  }

  momentProcess(timeDate) {
    return moment(timeDate).fromNow();
  }

  async editOpen(item) {
    // console.log(item);
    this.common.dataBuff = item;
    this.common.indexInd = item.index;

    this.openModal.emit();
  }
}
