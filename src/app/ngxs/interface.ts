export type ISODate = string;
export interface JobModel {
  title: string;
  location: string;
  department: string;
  level?: string;
  createdAt: ISODate;
  updatedAt: ISODate;
  index: number;
}
export interface DepartmentModel {
  name: string;
}
