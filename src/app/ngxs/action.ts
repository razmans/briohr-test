export class POPULATE_DATA {
  static readonly type = `[COMFORT WORKS] GET DATA`;
  constructor() {}
}

export class SEARCH_JOB {
  static readonly type = `[COMFORT WORKS] SEARCH JOB`;
  constructor(public readonly payload: string) {}
}

export class SEARCH_JOB_DEPT {
  static readonly type = `[COMFORT WORKS] SEARCH JOB DEPT`;
  constructor(public readonly payload: string) {}
}

export class EDIT_DATA {
  static readonly type = `[COMFORT WORKS] EDIT MODAL DATA`;
  constructor(public readonly payload) {}
}
