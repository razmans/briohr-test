import { Injectable } from '@angular/core';
import { JobModel, ISODate, DepartmentModel } from './interface';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { initialData } from './initialData';
import {
  POPULATE_DATA,
  SEARCH_JOB,
  SEARCH_JOB_DEPT,
  EDIT_DATA,
} from './action';
import { LoadingController } from '@ionic/angular';

interface DataModel {
  data: Array<JobModel>;
}

@State<DataModel>({
  name: 'JobState',
  defaults: {
    data: [],
  },
})
@Injectable()
export class JobState {
  constructor(private loading: LoadingController) {}

  @Selector()
  public static allDataCount(state: DataModel) {
    return state.data.length;
  }

  @Selector()
  public static marketingData(state: DataModel) {
    return state.data.filter((res) => {
      return res.department == 'Marketing';
    });
  }

  @Selector()
  public static marketingDataCount(state: DataModel) {
    return state.data.filter((res) => {
      return res.department == 'Marketing';
    }).length;
  }

  @Selector()
  public static othersData(state: DataModel) {
    return state.data.filter((res) => {
      return res.department != 'Marketing';
    });
  }

  @Selector()
  public static othersDataCount(state: DataModel) {
    return state.data.filter((res) => {
      return res.department != 'Marketing';
    }).length;
  }

  @Action(POPULATE_DATA)
  public async populateData({ patchState }: StateContext<DataModel>) {
    return patchState({
      data: initialData,
    });
  }

  @Action(SEARCH_JOB)
  public async searchJob(
    { patchState, getState }: StateContext<DataModel>,
    { payload }: SEARCH_JOB
  ) {
    // console.log('SEARCH DATA', payload);
    let currentData = initialData;
    let newData = currentData.filter((res) => {
      return res.title.toLowerCase().indexOf(payload.toLowerCase()) > -1;
    });

    // console.log(newData);
    return patchState({
      data: newData,
    });
  }

  @Action(SEARCH_JOB_DEPT)
  public async searchJobDept(
    { patchState, getState }: StateContext<DataModel>,
    { payload }: SEARCH_JOB
  ) {
    // console.log('SEARCH DATA', payload);
    let currentData = initialData;
    let newData;
    if (payload == 'allDepartment') {
      newData = initialData;
    } else if (payload == 'marketing') {
      newData = currentData.filter((res) => {
        return res.department.toLowerCase() == 'marketing';
      });
    } else if (payload == 'others') {
      newData = currentData.filter((res) => {
        return res.department.toLowerCase() != 'marketing';
      });
    }

    // console.log(newData);
    return patchState({
      data: newData,
    });
  }

  @Action(EDIT_DATA)
  public async editData(
    { patchState, getState }: StateContext<DataModel>,
    { payload }: EDIT_DATA
  ) {
    const index = payload.index;
    const currentState = getState();
    let currentData = currentState.data;

    for (var x = 0; x < currentData.length; x++) {
      if (currentData[x].index == payload.index) {
        currentData[x] = {
          ...currentData[x],
          ...payload,
        };
      }
    }

    return patchState({
      data: currentData,
    });
  }
}
